# EventPoll socket_server

## Installation

In order to build the socket_server, please install the following requirements :
- Install [make](https://tldp.org/HOWTO/Software-Building-HOWTO-3.html)

At the repository's root, execute :
```bash
make install
```

## Presentation
The purpose of this project is to build a socket_server on top of the [epoll api](https://linux.die.net/man/7/epoll).


Please do **mind** that this code doesn't aim to be used in a production build.s


## Conception

TODO

## Contribute

Feel free to contribute by sending merge request.