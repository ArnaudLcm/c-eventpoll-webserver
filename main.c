#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "config.h"
#include "logger.h"
#include "socket_server.h"

int request_processer(char *input, int client_fd, int server_error_code)
{
    if (server_error_code != SE_NONE)
    {
        char *error_message = "Server-side error : Unable to treat request.";
        if (write(client_fd, error_message, (strlen(error_message) + 1) * sizeof(char)) < 0)
            return -1;
        return 0;
    }

    int err = write(client_fd, input, (strlen(input) + 1) * sizeof(char));
    if (err < 0)
    {
        return -1;
    }
    return 0;
}

int main(int argc, char *argv[])
{
    init();

    return socket_server_start(request_processer);
}