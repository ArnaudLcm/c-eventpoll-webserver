#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include "tests.h"
#include "test_example.h"
#include "test_socket_server.h"

int main(int argc, char *argv[])
{
    init();

    new_test_batch("test_example_batch1");
    test_example_batch();
    end_test_batch();

    new_test_batch("test_example_batch2");
    test_example_batch();
    end_test_batch();

    new_test_batch("test_connection_socket_server");
    test_connection_batch();
    end_test_batch();

    return EXIT_SUCCESS;
}