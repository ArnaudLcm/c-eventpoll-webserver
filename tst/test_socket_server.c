#include <sys/types.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <poll.h>

#define h_addr h_addr_list[0] /* for backward compatibility */

#include "test_socket_server.h"
#include "tests.h"
#include "logger.h"
#include "config.h"
#include "socket_server.h"

#define TEST_CLEAN clean_test_connection_batch(socket_fd, pid)

char *IDENTITY(char *arg)
{
    return arg;
}

void clean_test_connection_batch(int socket_fd, int pid)
{
    close(socket_fd);

    log_debug("%d", kill(pid, SIGKILL));
}

void test_connection_batch()
{
    int pid;
    if ((pid = fork()) == 0) // If in child process
    {
        log_debug("ChildProcess");
        // log_debug("Return Code: %d", socket_server_start(IDENTITY));
        log_debug("Return Code: %d", socket_server_start());
        exit(0);
    }
    log_debug("ParentProcess");
    sleep(3);

    int socket_fd;
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd < 0)
    {
        log_error("TEST_CLIENT: Socket opening failed");
        ASSERT_TRUE_FATAL(0)
    }

    char *host = "localhost";
    struct hostent *server = gethostbyname(host);
    if (server == NULL)
    {
        log_error("TEST_CLIENT: Host not found : %s", host);
        ASSERT_TRUE_FATAL(0)
    }

    struct sockaddr_in server_addr;
    // Empty the addr memory zone
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    memcpy((char *)server->h_addr,
           (char *)&server_addr.sin_addr.s_addr,
           server->h_length);
    server_addr.sin_port = htons(PORT);

    if (connect(socket_fd, &server_addr, sizeof(server_addr)) < 0)
    {
        log_error("TEST_CLIENT: Connection to server failed");
        ASSERT_TRUE_FATAL(0)
    }

    char *input = "Test Message To Send";
    int n = write(socket_fd, input, strlen(input));
    if (n < 0)
    {
        log_error("TEST_CLIENT: Write to socket file descriptor failed");
        ASSERT_TRUE_FATAL(0)
    }
    log_debug("TEST_CLIENT: Write done");

    struct pollfd pfd[1];
    pfd[0].fd = socket_fd;
    pfd[0].events = POLLIN;

    int ret = poll(pfd, 1, 5000);
    if (ret < -1)
    {
        log_error("TEST_CLIENT: Poll failed");
        ASSERT_TRUE_FATAL(0)
    }
    else if (ret == 0)
    {
        // Time expired
        ASSERT_TRUE_FATAL(0)
    }
    else
    {
        // Read possible
        char buffer[256];
        memset(buffer, 0, 256);
        n = read(socket_fd, buffer, 255);
        if (n < 0)
        {
            log_error("TEST_CLIENT: Reading from socket file descriptor failed");
            ASSERT_TRUE_FATAL(0)
        }
        log_debug("Client Received: %s\n", buffer);
        ASSERT_TRUE_FATAL(strcmp(buffer, input) == 0)
    }
}

#undef TEST_CLEAN