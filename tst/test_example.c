#include "test_example.h"
#include "tests.h"

void test_example_batch() {
    test_feature_1();
    test_feature_2();
}

void test_feature_1() {
    ASSERT_TRUE(2==2);
    ASSERT_TRUE(2==2);
    ASSERT_TRUE(2==2);
}

void test_feature_2() {
    ASSERT_TRUE(2==2);
    ASSERT_TRUE(3==3);
}