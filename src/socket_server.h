#ifndef __SOCKET_SERVER_H__
#define __SOCKET_SERVER_H__

enum ServerInternalErrorCode {
    SE_NONE,
    SE_UNAVAILABLE_THREAD,
};

int socket_server_start(int (*request_processer)(char *, int connection_socket, int server_error_code));

void *fill_epoll(void *arg);

int socket_setup();

int epoll_setup();

int socket_server_start();

void sigint_handler();

extern int (*server_request_processer)(char *, int, int);

typedef struct
{
    int socket_fd;
    int epoll_fd;
} socket_epoll_provision;

#endif // __SOCKET_SERVER_H__