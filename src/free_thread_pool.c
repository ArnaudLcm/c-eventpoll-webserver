#include <signal.h>

#include "free_thread_pool.h"
#include "dispatcher.h"
#include "busy_thread_pool.h"

thread_group **free_thread_pool = NULL;

int free_stack_index = -1;

pthread_mutex_t mutex_free_thread_pool;

void feed_thread_group()
{
    thread_group *thread_stack_elt = malloc(sizeof(thread_group));

    int pipefd[2];
    pipe(pipefd);

    thread_stack_elt->metadatas.pipe_read_fd = pipefd[0];
    thread_stack_elt->metadatas.pipe_write_fd = pipefd[1];
    thread_stack_elt->metadatas.connection_socket_fd = -1;
    thread_stack_elt->metadatas.node = NULL;

    push(thread_stack_elt);

    pthread_create(&thread_stack_elt->thread, NULL, thread_create_handler, thread_stack_elt);
}

int init_free_thread_pool()
{
    log_info("Initializing thread pool...");

    free_thread_pool = malloc(sizeof(thread_group *) * INIT_THREAD_POOL_SIZE);

    if (free_thread_pool == NULL)
    {
        log_error("Can't allocate the free thread pool.");
        return EXIT_FAILURE;
    }

    for (size_t i = 0; i < INIT_THREAD_POOL_SIZE; i++)
    {
        feed_thread_group();
    }

    return pthread_mutex_init(&mutex_free_thread_pool, NULL);
}

void clean_free_threads()
{
    while (free_stack_index != -1)
    {
        thread_group *tg = pop();
        write(tg->metadatas.pipe_write_fd, "kill", 5 * sizeof(char));
        clean_thread_group(tg);
    }
    free(free_thread_pool);
}

thread_group *pop()
{
    pthread_mutex_lock(&mutex_free_thread_pool);

    if (free_stack_index == -1)
    {
        return NULL;
    }

    int i = free_stack_index;
    free_stack_index -= 1;
    pthread_mutex_unlock(&mutex_free_thread_pool);

    return free_thread_pool[i];
}

int push(thread_group *p)
{
    pthread_mutex_lock(&mutex_free_thread_pool);

    free_thread_pool[free_stack_index + 1] = p;

    free_stack_index++;
    pthread_mutex_unlock(&mutex_free_thread_pool);

    return EXIT_SUCCESS;
}

void expand()
{
    int current_thread_pool_size = free_stack_index + busy_list_size;

    free_thread_pool = realloc(free_thread_pool, (current_thread_pool_size * 2) * sizeof(thread_group));
    if (free_thread_pool == NULL)
    {
        log_error("Can't expand free thread pool stack");
        return;
    }
    for (size_t i = 0; i < current_thread_pool_size; i++)
    {
        feed_thread_group();
    }

    return;
}

void shrink()
{
    int current_thread_pool_size = free_stack_index + 1 + busy_list_size;

    free_thread_pool = realloc(free_thread_pool, ((current_thread_pool_size) / 2) * sizeof(thread_group));
    if (free_thread_pool == NULL)
    {
        log_error("Can't expand free thread pool stack");
        return;
    }

    for (size_t i = 0; i < current_thread_pool_size / 2; i++)
    {
        thread_group *tg = pop();
        clean_thread_group(tg);
        write(tg->metadatas.pipe_write_fd, "kill", 5 * sizeof(char));
    }

    return;
}

void *show()
{

    for (size_t i = free_stack_index; i > 0; i--)
    {
        log_info("Stack index : %d", free_stack_index);
    }

    return NULL;
}

int stack_init()
{
    free_thread_pool = (thread_group **)malloc(sizeof(thread_group *) * INIT_THREAD_POOL_SIZE);

    return EXIT_SUCCESS;
}