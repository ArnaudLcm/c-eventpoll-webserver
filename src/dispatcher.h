#ifndef __DISPATCHER_H__
#define __DISPATCHER_H__

#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdio.h>
#include "pthread.h"

typedef struct
{
    int pipe_read_fd;
    int pipe_write_fd;
    int connection_socket_fd;
    struct node *node;
} metadata;


typedef struct
{
    pthread_t thread;
    metadata metadatas;
} thread_group;

void clean_thread_group(thread_group* tg);


extern struct node *tail_busy_thread_pool;

void* thread_create_handler(void* arg);

void dispatcher_close();

thread_group *dispatcher_get_available();

#endif //!__DISPATCHER_H__