#include <stdlib.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <errno.h>
#include "pthread.h"
#include <sys/epoll.h>

#include "socket_server.h"
#include "logger.h"
#include "config.h"
#include "dispatcher.h"
#include "free_thread_pool.h"
#include "busy_thread_pool.h"

#define CONNECTION_QUEUE_MAX_SIZE 10

int (*server_request_processer)(char *, int, int) = NULL;
int socket_fd = -1;
int epoll_fd = -1;
pthread_t* thread_fillepoll;


/**
 * @brief Initialize the socket stream and bind it to the configured port and IPv4 address
 */
int socket_setup()
{
    // See man socket for parameters explanation
    socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_fd == -1)
    {
        log_error("Socket opening failed");
        return EXIT_FAILURE;
    }

    // Set an option on the socket in order to allow multiple uses on the same address
    // Fix the bug where we can't run make tests too fast
    const int enable = 1;
    if (setsockopt(socket_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)))
    {
        log_error("Can't set socket option SO_REUSEADDR to 1");
    }

    struct sockaddr_in addr;
    // Empty the addr memory zone
    memset(&addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_addr.s_addr = INADDR_ANY;
    addr.sin_port = htons(PORT);

    if (bind(socket_fd, (struct sockaddr *)&addr, sizeof(addr)) == -1)
    {
        log_error("Socket binding failed : %s", strerror(errno));
        return EXIT_FAILURE;
    }
    log_success("Socket is bound to port: %d", PORT);

    if (listen(socket_fd, CONNECTION_QUEUE_MAX_SIZE) == -1)
    {
        log_error("Failed to listen on socket");
        return EXIT_FAILURE;
    }
    log_success("Now listening to the socket.. (%d connections max)", CONNECTION_QUEUE_MAX_SIZE);
    return EXIT_SUCCESS;
}

/**
 * @brief This function is called is a separate thread in order to loop on every incoming
 * request on the socket and then feed the epoll listener_lists with the associated file
 * descriptors.
 */
void *fill_epoll(void *arg)
{
    struct epoll_event ev;
    ev.events = EPOLLIN | EPOLLET; // Event type which will trigger epoll_wait

    for (;;)
    {
        struct sockaddr_in client_addr;
        socklen_t client_addr_size = sizeof(client_addr);
        int connection_socket_fd = accept(socket_fd, (struct sockaddr *)&client_addr, &client_addr_size);
        if (connection_socket_fd == -1)
        {
            log_error("Failed to accept a connection on socket : %s", strerror(errno));
            continue;
        }

        char printable_addr[INET_ADDRSTRLEN]; // Will be used to format the IpV4 adress into xxxx.xxxx.xxxx.xxxx

        inet_ntop(AF_INET, &client_addr.sin_addr, printable_addr, sizeof(printable_addr));
        log_success("Connection accepted, client addr: %s", printable_addr);

        ev.data.fd = connection_socket_fd;
        if (epoll_ctl(epoll_fd, EPOLL_CTL_ADD, connection_socket_fd, &ev) == -1)
        {
            log_error("Failed to bind an incoming request to the epoll listener list.");
            continue;
        }
    }
    return NULL;
}

/**
 * @brief Initialize the epoll file descriptor and run in an other thread the function that loop
 * on every incoming request (fill_epoll())
 */
int epoll_setup()
{
    thread_fillepoll = malloc(sizeof(pthread_t));
    epoll_fd = epoll_create1(0);
    if (pthread_create(thread_fillepoll, NULL, &fill_epoll, NULL) == -1)
    {
        log_error("Can't create epoll thread provision");
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

/**
 * @brief This function will initialize the stream socket we are listening to and then wait for
 * every EPOLLET event on incoming request to process.
 *
 * @param request_processer Function that process a request (given by a client) to a specific output to send
 *      Note : The third parameter of this function is a server_error_code. If there is a problem while handling user request
 *             such as a lack of thread, this parameter will have a value different from SE_NONE. In those cases, keep in mind that
 *             request_processer is called in the main thread (the one calling epoll_wait) and so, you should not do a heavy time
 *             consuming error handling.
 */
int socket_server_start(int (*request_processer)(char *, int connection_socket, int server_error_code))
{
    signal(SIGINT, sigint_handler);

    server_request_processer = request_processer;

    init_busy_thread_pool();
    init_free_thread_pool();

    if (socket_setup())
        return EXIT_FAILURE;

    if (epoll_setup())
        return EXIT_FAILURE;

    log_info("Server is now listening to incoming requests...");
    struct epoll_event events[1];
    for (;;)
    {
        int err = epoll_wait(epoll_fd, events, 1, -1);
        if (err == -1)
        {
            log_error("An error occured while waiting for an incoming request: epoll_wait");
        }

        thread_group *available_thread = dispatcher_get_available();
        if (available_thread == NULL)
        {
            log_error("Can't handle this request : no thread available");
            server_request_processer(NULL, events[0].data.fd, SE_UNAVAILABLE_THREAD);
        }

        available_thread->metadatas.connection_socket_fd = events[0].data.fd;

        struct node *node = push_tail(available_thread); // Switch thread to thread busy list

        available_thread->metadatas.node = node;
        write(node->value->metadatas.pipe_write_fd, "", sizeof(char));
    }
    return EXIT_FAILURE;
}

/**
 * @brief Function used to handle INT signal in order to terminate the server and clean memory
 */
void sigint_handler()
{
    dispatcher_close();
    free(thread_fillepoll);
    log_success("Server closed");
    exit(EXIT_SUCCESS);
}
