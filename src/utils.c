#include "utils.h"
#include "config.h"
#include <sys/time.h>

/**
 * This macros determine the estimated time for the cpu in microseconds to firstly create/delete a thread and maintain a thread
 * alive (so mainly the cost associated with thread context switching)
*/
#define ESTIMATED_COST_MS_CREATE_THREAD 1.0
#define ESTIMATED_COST_MS_SWITCH_CONTEXT_THREAD 0.5


/**
 * This macro determines during how many time do we have to save the thread used information during a sample creation
*/
#define SAMPLING_PERIOD_THREADS_USED 200 // In microseconds


/**
 * This macro determines how many measures do we have to make during the SAMPLING_PERIOD_THREADS_USED
*/
#define SAMPLE_NBR_MEASURES 200

/**
 * @brief The purpose of this function is to determine the probability to get i threads occuped at a time slice
 * @note In order to work, the function needs a data array. This data array has to contain the evolution of used threads
 * for a certain period t.
*/
int* probability_law_busy_threads(int* filled_occurency) {
    int total = 0; // Nbr total of threads sampled

    int* probabilities_period_threads = malloc(SAMPLING_PERIOD_THREADS_USED * sizeof(int));

    for (int i = 0; i < SAMPLING_PERIOD_THREADS_USED; i++)
    {
        total += filled_occurency[i];
        probabilities_period_threads[filled_occurency[i]]++;
        
    }

    for(int i = 0; i < MAX_THREAD_POOL_SIZE; i++) {
        probabilities_period_threads[i] = probabilities_period_threads[i]/MAX_THREAD_POOL_SIZE; // @TODO : prevent float creation
    }

    return probabilities_period_threads;
    
}


int* collect_thread_used() {
    
    int* filled_occurency = malloc(SAMPLE_NBR_MEASURES*sizeof(int));
    struct timeval start_tv;

    struct timeval current_tv;

    gettimeofday(&start_tv, NULL);
    gettimeofday(&current_tv, NULL);

    while(current_tv.tv_usec - start_tv.tv_usec < SAMPLING_PERIOD_THREADS_USED) {
        // @TODO


    }

    return filled_occurency;

}





int get_optimized_free_thread_pool_size() {
    int* filled_occurency = collect_thread_used();
    int* probabilities_period_threads = probability_law_busy_threads(filled_occurency);

    double cp = 0.0;
    int optimal_thread_size = 0;

    for (size_t i = 0; i < MAX_THREAD_POOL_SIZE; i++)
    {
        cp += probabilities_period_threads[i];
        if(cp >= 1.0 - ESTIMATED_COST_MS_SWITCH_CONTEXT_THREAD/ESTIMATED_COST_MS_CREATE_THREAD) {
            optimal_thread_size = i;
            break;
        }
    }

    return optimal_thread_size;
    
}

