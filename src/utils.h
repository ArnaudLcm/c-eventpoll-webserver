#ifndef UTILS_H
#define UTILS_H

#include "dispatcher.h"
#include "logger.h"

int* probability_law_busy_threads(int* filled_occurency);

#endif