#ifndef __BUSY_THREAD_POOL_H__
#define __BUSY_THREAD_POOL_H__

#include "dispatcher.h"

struct node {
    thread_group* value;
    struct node* next;
    struct node* prev;
};

extern int busy_list_size;

int init_busy_thread_pool();

void clean_nodes();

struct node* push_tail(thread_group* available_thread);

void remove_node(struct node* n);

extern struct node* tail_busy_thread_pool;

#endif //!__BUSY_THREAD_POOL_H__