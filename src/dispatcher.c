#include "dispatcher.h"
#include "config.h"
#include "logger.h"
#include <stdlib.h>
#include "socket_server.h"
#include "utils.h"
#include <unistd.h>
#include "free_thread_pool.h"
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "busy_thread_pool.h"

#define REQUEST_BUFF_SIZE 256

struct node *tail_busy_thread_pool = NULL;

/**
 * This is the function called directly after adding a new thread in the thread pool.
 * This thread will be blocked by read system call
 * When this thread is allocated to an incoming request, he will be set to busy and process the request.
 * This write operation on the metadata pipe filedescriptor will unblock the thread
 */
void *thread_create_handler(void *arg)
{
    thread_group *thread_stack_elt = (thread_group *)arg;

    char buff[REQUEST_BUFF_SIZE];
    char input[REQUEST_BUFF_SIZE];

    for (;;)
    {
        // Reset buffers content
        for (int i = 0; i < REQUEST_BUFF_SIZE; i++)
        {
            buff[i] = 0;
            input[i] = 0;
        }

        int read_res = read(thread_stack_elt->metadatas.pipe_read_fd, buff, REQUEST_BUFF_SIZE);
        if (read_res == -1)
        {
            log_error("Reading from activaton file descriptor failed");
        }
        else if (read_res == 0)
        {
            log_warning("Reading from file descriptor reached end of file (EOF)");
        }

        // log_debug("BUFF: %s", buff);
        if (strcmp(buff, "kill") == 0) // In the case where we don't want to process a request but kill the thread
        {
            sleep(3);
            // log_debug("Thread killed : %p", thread_stack_elt->thread);
            pthread_exit(0);
        }

        if (-1 == read(thread_stack_elt->metadatas.connection_socket_fd, input, REQUEST_BUFF_SIZE))
        {
            log_warning("Reading from connection file descriptor failed (A client probably violently ended a connection)");
        }

        if (-1 == server_request_processer(input, thread_stack_elt->metadatas.connection_socket_fd, SE_NONE))
        {
            log_error("Can't write response to a specific client");
        }


        remove_node(thread_stack_elt->metadatas.node);
        push(thread_stack_elt);

        int current_thread_pool_size = free_stack_index + 1 + busy_list_size;
        // Check if the free thread pool size is not used enough : in this case, shrink it
        if ((busy_list_size * 1.0) / (current_thread_pool_size) < BOTTOM_THRESHOLD_POOL && INIT_THREAD_POOL_SIZE <= current_thread_pool_size / 2)
        {
            pthread_t thread_shrink;
            pthread_create(&thread_shrink, NULL, (void *(*)(void *))shrink, NULL);
        }
    }
}

/**
 * @brief The purpose of this function is to retrieve a free thread pool
 */
thread_group *dispatcher_get_available()
{

    thread_group *tg = pop();

    int current_thread_pool_size = free_stack_index + 1 + busy_list_size;

    if (busy_list_size / (current_thread_pool_size) > TOP_THRESHOLD_POOL && MAX_THREAD_POOL_SIZE >= current_thread_pool_size * 2)
    {
        pthread_t thread_expand;
        pthread_create(&thread_expand, NULL, (void *(*)(void *))expand, NULL);
    }

    return tg;
}

/**
 * @brief Prevent memory leak with unallocation.
 */
void dispatcher_close()
{
    clean_nodes();
    clean_free_threads();
}

void clean_thread_group(thread_group *tg)
{
    if (!tg)
        return;

    if (-1 != tg->metadatas.connection_socket_fd && close(tg->metadatas.connection_socket_fd))
    {
        log_error("Can't close connectection socket on clean thread group : %s", strerror(errno));
    }

    if (close(tg->metadatas.pipe_read_fd))
    {
        log_error("Can't close pipe read file descriptor on clean thread group : %s", strerror(errno));
    }

    if (close(tg->metadatas.pipe_write_fd))
    {
        log_error("Can't close pipe write file descriptor on clean thread group : %s", strerror(errno));
    }

    free(&tg->thread);
    free(tg);
}