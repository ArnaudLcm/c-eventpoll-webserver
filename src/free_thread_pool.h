#ifndef __FREE_THREAD_POOL_H__
#define __FREE_THREAD_POOL_H__

#include "dispatcher.h"
#include "stdlib.h"
#include "stdio.h"
#include "logger.h"
#include "config.h"

extern thread_group **free_thread_pool;
extern int free_stack_index;

int stack_init();

void expand();

void clean_free_threads();

void shrink();

int init_free_thread_pool();

thread_group *pop();

int push(thread_group *p);

void *show();

#endif //!__FREE_THREAD_POOL_H__