#ifndef __CONFIG_H__
#define __CONFIG_H__

extern int LOGGING_LEVEL;
extern int FILES_LOGGING;
extern int PORT;
extern int MAX_THREAD_POOL_SIZE;
extern int INIT_THREAD_POOL_SIZE;
extern double TOP_THRESHOLD_POOL;
extern double BOTTOM_THRESHOLD_POOL; 

void init();

#endif // __CONFIG_H__