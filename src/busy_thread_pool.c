#include "busy_thread_pool.h"
#include "dispatcher.h"


pthread_mutex_t mutex_busy_thread_pool;

int busy_list_size = 0; // Used in order to decide whenever the free thread pool has to expand or shrink

int init_busy_thread_pool() {
    return pthread_mutex_init(&mutex_busy_thread_pool, NULL);
}

/**
 * @brief When a thread is used by an user in order to handle his request,
 * his status switch from free to busy. In order to do this switch,
 * the thread group associated to the request has to be take out from
 * the free thread pool (a stack) and pushed at the end of this double linked list
 * (the busy thread pool)
*/
struct node* push_tail(thread_group* available_thread) {

    pthread_mutex_lock(&mutex_busy_thread_pool); // Prevent concurent access on this write action

    busy_list_size++;


    struct node* node = malloc(sizeof(struct node));
    

    node->value = available_thread;
    node->prev = tail_busy_thread_pool;
    node->next = NULL;

    if(tail_busy_thread_pool == NULL) { // Check if it's the first element of the list
        tail_busy_thread_pool = node;
    } else {
        tail_busy_thread_pool->next = node;
        tail_busy_thread_pool = node;
    }
    pthread_mutex_unlock(&mutex_busy_thread_pool);

    return node;


}

void clean_nodes() {
    while(tail_busy_thread_pool) {
        tail_busy_thread_pool = tail_busy_thread_pool->prev;
        clean_thread_group(tail_busy_thread_pool->next->value);
        free(tail_busy_thread_pool->next);
    }
    
}


/**
 * @brief See push_tail brief : it is the reverse operation
*/
void remove_node(struct node* n) {

    pthread_mutex_lock(&mutex_busy_thread_pool);

    busy_list_size--;

    if(n->prev) {
        n->prev->next = n->next;

    }

    if(n->next) {
        n->next->prev = n->prev;
    } else {
        tail_busy_thread_pool = n->prev;
    }

    free(n);

    pthread_mutex_unlock(&mutex_busy_thread_pool);
}
