#include <stdlib.h>

#include "config.h"

int LOGGING_LEVEL = 0;
int FILES_LOGGING = 0;
int PORT = 0;
int INIT_THREAD_POOL_SIZE = 0;
int MAX_THREAD_POOL_SIZE = 0;
double TOP_THRESHOLD_POOL = 0.0;
double BOTTOM_THRESHOLD_POOL = 0.0; 

/**
 * @brief Initialize global variables depending on environment variables
 */
void init()
{
    LOGGING_LEVEL = atoi(getenv("LOGGING_LEVEL"));
    FILES_LOGGING = atoi(getenv("FILES_LOGGING"));
    PORT = atoi(getenv("CONTROLLER_PORT"));
    INIT_THREAD_POOL_SIZE = atoi(getenv("INIT_THREAD_POOL_SIZE"));
    MAX_THREAD_POOL_SIZE = atoi(getenv("MAX_THREAD_POOL_SIZE"));
    TOP_THRESHOLD_POOL = atof(getenv("TOP_THRESHOLD_POOL"));
    BOTTOM_THRESHOLD_POOL = atof(getenv("BOTTOM_THRESHOLD_POOL"));
}