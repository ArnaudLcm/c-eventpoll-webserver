CC=gcc
CFLAGS=-D_GNU_SOURCE -Wall
BUILD=build
SOURCES=src
TESTS=tst
INC=-I src

# Generate names for object files depending on source files found
SRC_OBJECTS:=$(shell ls $(SOURCES)/*.c | sed 's/\.h\|\.c/.o/g' | uniq | sed 's-$(SOURCES)/-$(BUILD)/-g')
TST_OBJECTS:=$(shell ls $(TESTS)/*.c | grep -v 'tests_main.c' | sed 's/\.h\|\.c/.o/g' | uniq | sed 's-$(TESTS)/-$(BUILD)/-g')

TOCLEAN=build build/*.o socket_server

# Import configuration file constants
include socket_server.cfg
export

all: build run

# Create mandatory directories
init:
	@mkdir -p $(BUILD)
	@mkdir -p logs

build: init socket_server

run: build
	./socket_server

debug: build
	valgrind ./socket_server

debug_full: build
	valgrind --leak-check=full ./socket_server

tests: init socket_server_tests
	valgrind ./socket_server_tests

# Build executable
socket_server: main.c $(SRC_OBJECTS)
	@CMD="$(CC) $(INC) -o $@ $^ $(CFLAGS)" && echo $$CMD && $$CMD && echo "\033[0;32m➜ Compilation successfully terminated.\033[0m" 

# Build test executable
socket_server_tests: $(TESTS)/tests_main.c $(SRC_OBJECTS) $(TST_OBJECTS)
	@CMD="$(CC) $(INC) -o $@ $^ $(CFLAGS)" && echo $$CMD && $$CMD && echo "\033[0;32m➜ Compilation successfully terminated.\033[0m" 	

# All tst/test*.c sources will be compiled to build/test*.o objects 
# This target has a higher priority than the one under it
$(BUILD)/test%.o: $(TESTS)/test%.c $(TESTS)/test%.h
	$(CC) $(INC) -o $@ -c $< $(CFLAGS)

# All src/*.c sources will be compiled to build/*.o objects
$(BUILD)/%.o: $(SOURCES)/%.c $(SOURCES)/%.h
	$(CC) $(INC) -o $@ -c $< $(CFLAGS)

clean:
	@filesCount=$$(ls $(TOCLEAN) 2>/dev/null | wc -l) && rm $(TOCLEAN) 2>/dev/null ; echo "$$filesCount files cleaned (\"$(TOCLEAN)\")."

libsocketserver.a: $(BUILD)/busy_thread_pool.o $(BUILD)/free_thread_pool.o $(BUILD)/dispatcher.o $(BUILD)/socket_server.o
	@CMD="ar -rcs $@ $^" && echo $$CMD && $$CMD && echo "\033[0;32m➜ Compilation successfully terminated.\033[0m"

install: init libsocketserver.a

.PHONY: clean build init run debug tests install